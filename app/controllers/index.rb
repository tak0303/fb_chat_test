FacebookChatTest.controllers :index do
  get :index, :map => "/" do
    render 'index/index'
  end

  post :chat do
    message = params[:message]
    sender_chat_id = "-100001478177771@chat.facebook.com"
    receiver_chat_id = "-1101664516@chat.facebook.com"
    message_body = message
    message_subject = 'hogehoge'

    jabber_message = Jabber::Message.new(receiver_chat_id, message_body)
    jabber_message.subject = message_subject

    client = Jabber::Client.new(Jabber::JID.new(sender_chat_id))
    client.connect
    client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client,
       '413618855381947', session['account']['credentials']['token'],
       '3f5cf7b6ad3ef7738f9ec62045e655bb'), nil)
    client.send(jabber_message)
    client.close
  end

  get :auth, :map => '/auth/:provider/callback' do
    auth = request.env['omniauth.auth']
    session['account'] = auth
    redirect '/'
  end
end